package arun.learn;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class App
{
    private static Logger logger = LoggerFactory.getLogger(App.class);
    public static void main( String[] args )
    {
        logger.info("Arun \r\n{\"thread\":\"main\",\"level\":\"INFO\",\"loggerName\":\"arun.learn.App\",\"message\":\"Arun \\r\\n2019-02-01T21:29:07,557 INFO  a.l.App: kumar. http://localhost:8080\",\"endOfBatch\":false,\"loggerFqcn\":\"org.apache.logging.slf4j.Log4jLogger\",\"instant\":{\"epochSecond\":1549058698,\"nanoOfSecond\":825505000},\"threadId\":1,\"threadPriority\":5,\"rid\":\"${ctx:x-allianz-request-id}\",\"jid\":\"${ctx:x-allianz-journey-id}\",\"cid\":\"${ctx:x-allianz-client-id}\"}\n");

        System.out.println(ESAPI.encoder().encodeForHTML("Arun \r\n text"));
        
    }
}
